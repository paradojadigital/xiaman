import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'font-awesome/css/font-awesome.css'
import 'jquery'
import 'bootstrap/dist/js/bootstrap'
import VueLocalStorage from 'vue-ls';


var options = {
  namespace: 'xiaman'
};

Vue.use(VueLocalStorage, options);

var $ = require('jquery');
window.jQuery = $;
window.$ = $;

//import '@/styles/app'

Vue.use(BootstrapVue)

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  render: h => h(App),
  mounted: function() {
   }
})
