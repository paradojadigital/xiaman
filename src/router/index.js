import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/home/Home'
import OurProcess from '../components/our-process/OurProcess'
import OurHistory from '../components/our-history/OurHistory'
import Xiaman from '../components/xiaman/Xiaman'
import PaymentSuccess from '../components/payment-success/PaymentSuccess'
import PaymentError from '../components/payment-error/PaymentError'
import Agb from '../components/agb/Agb'
import Datenschutz from '../components/datenschutz/Datenschutz'
import RuckgabeUmtausch from '../components/ruckgabe-umtausch/RuckgabeUmtausch'
import Impressum from '../components/impressum/Impressum'

Vue.use(Router)

function route (path, name, component) {
  return { path, name, component }
}

const router = new Router({
  //base: 'https://poldiaz.com/clientes/xiaman/',
  linkActiveClass: 'route-active',
  linkExactActiveClass: 'route-active-exact',
  routes: [
    route('/:country/', 'home', Home),
    route('/:country/xiaman', 'xiaman', Xiaman),
    route('/:country/our-process', 'our-process', OurProcess),
    route('/:country/our-history', 'our-history', OurHistory),

    route('/:country/payment-success', 'payment-success', PaymentSuccess),
    route('/:country/payment-error', 'payment-error', PaymentError),

    route('/:country/agb', 'agb', Agb),
    route('/:country/datenschutz', 'datenschutz', Datenschutz),
    route('/:country/ruckgabe-umtausch', 'ruckgabe-umtausch', RuckgabeUmtausch),
    route('/:country/impressum', 'impressum', Impressum),
  ]
})

router.beforeEach((to, from, next) => {
  // After enter a route scroll to top.
  window.scrollTo(0, 0)
  next()
})

// Export the route module.
export default router
